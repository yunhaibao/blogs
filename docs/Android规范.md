[toc]
# <center> Android规范 </center>

<center>

版本 | 内容| 日期| 作者
 :-: | :-: | :-:| :-:
V1.0|创建Android代码规范|2020-03-22|姚鑫

</center>

# 说明呀d
- 其他未做说明参照<<Caché代码规范>>。
- Andorid端与<<Caché代码规范>>冲突的内容按照此文档为准。

# 类
1. Activity命名：以Activity作为后缀。

反例：
```
    ActivityBase
```
正例：
```
    BaseActivity
```

2. Adapter命名：以Adapter作为后缀。

反例：
```
    AdapterBase
```
正例：
```
    BaseAdapter
```

3.Bean命名：以Bean作为后缀。

反例：
```
    BeanrBase
```
正例：
```
    BaseBean
```

4. Fragment命名：以Fragment作为后缀。

反例：
```
    FragmentHome
```
正例：
```
    HomeFragment
```

5. Service命名：以Service作为后缀。

反例：
```
    ServiceDownLoad
```
正例：
```
    DownLoadService
```

6. BroadReceive命名：以Receive作为后缀。

反例：
```
    BroadReceivePush
```
正例：
```
    PushReceive
```

7. ContentProvider命名：以Provider作为后缀。

反例：
```
    ContentProviderContract
```
正例：
```
    ContractProvider
```

# 资源文件命名
1. Activity布局：以Activity作为前缀。注意都是小写

反例：
```
    act_base.xml
```
正例：
```
    activtiy_base.xml
```

2. 列表中的item布局文件：以item作为国定前缀，列表项的名称作为后缀。注意都是小写

反例：
```
    itm_base.xml
```
正例：
```
    item_base.xml
```

3. Dialog布局文件:以dialog作为固定前缀。

反例：
```
    dlg_base.xml
```
正例：
```
    dialog_common.xml
```

4. 背景图片以bg作为前缀。

反例：
```
    background_white.xml
```
正例：
```
    bg_white.xml
```

5. 资源文件以文件后缀名开头.

反例：
```
    launch.png
```
正例：
```
    png_launch.png
```

# 控件命名
1. 以下表格加缩写作为前缀,加功能名称。

<center>

控件| 缩写
---|---
LinearLayout | ll
RelativeLayout| rl
TextView | tv
Button| btn
ImageButton | ibg
ImageView| iv
CheckBox | cb
RadioButton| rb
EditText | et
TimePicker| tp
ToggleButton | tb
ProgressBar| pb
WebView | wv
tabLayout | tl
ListView| lv
RecyclerView | rv

</center>

反例：
```
    RecyclerView_list
```
正例：
```
    rv_list
```

2. 布局空间ID全部小写，多个单词则用`_`分开。

反例：
```
    tv_addUser
```
正例：
```
    tv_add_user
```

# 方法

1. 方法全部用使用`lowerCamelCase`风格。并且以动词开头。

反例：
```
    InitList();
```
正例：
```
    initList();
```

# 风格
1. 要分门别类存放各种类。

例如：
- 工具类都放到utils包下。
- 数据库都放到database下。
![image](6E57BAA49DFC4EFB8A9932492D653092)